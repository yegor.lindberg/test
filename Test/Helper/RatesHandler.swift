//
//  RatesHandler.swift
//  Test
//
//  Created by Egor Lindberg on 28.03.2021.
//

import Foundation
import Services

final class RatesHandler {
    private var apiClient = Services.shared.mockApiClient
    
    func getStockRates(completion: @escaping ([StockRate]?) -> Void) {
        apiClient.fetchAllStocks()
            .done { [weak self] (data) in
                guard let self = self else { return }
                completion(self.parseStockRates(from: data))
            }
            .catch { (error) in
                print(error.localizedDescription)
                completion(nil)
            }
    }
    
    private func parseStockRates(from data: Data) -> [StockRate]? {
        let decoder = JSONDecoder()
//        decoder.dataDecodingStrategy
        if let stockRates = try? decoder.decode(StockRates.self, from: data) {
            return stockRates.rates
        }
        return nil
    }
    
}
