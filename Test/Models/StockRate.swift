//
//  StockRate.swift
//  Test
//
//  Created by Egor Lindberg on 28.03.2021.
//

struct StockRate: Codable, Hashable {
    var date: String
    var price: Int
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(date)
    }
}

struct StockRates: Codable {
    var rates: [StockRate]
}
