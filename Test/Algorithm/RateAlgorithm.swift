//
//  RateAlgorithm.swift
//  Test
//
//  Created by Egor Lindberg on 30.03.2021.
//

import Foundation

final class RateAlgorithm {
    
    enum RateStatus: String {
        case buy
        case sell
        case wait
    }
    
    public static func calculateProfit(for segment: [StockRate]) -> (rateStatus: [StockRate: String], profit: Int)
    {
        var profit = 0
        var rateStatus: [StockRate: String] = [:]
        segment.forEach { (rate) in
            rateStatus[rate] = RateStatus.wait.rawValue
        }
        guard segment.count >= 3 else {
            return (rateStatus, profit)
        }
        
        var isRateInHand = false
        var purchasedPrice = 0
        
        var beginIndexOfPart = 0
        var endIndexOfPart = 0
        
        for (index, rate) in segment.enumerated() {
            if index == 0 { continue }
            if (index == segment.count - 1)
                || isPit(mid: rate.price, left: segment[index - 1].price, right: segment[index + 1].price)
            {
                beginIndexOfPart = endIndexOfPart
                endIndexOfPart = index
                
                if isRateInHand {
                    let indexOfMaxElement = findFirstIndexWithMaxPriceIn(segment: segment,
                                                                         fromIndex: beginIndexOfPart,
                                                                         toIndex: endIndexOfPart)
                    if segment[indexOfMaxElement].price > purchasedPrice {
                        rateStatus[segment[indexOfMaxElement]] = RateStatus.sell.rawValue
                        profit += segment[indexOfMaxElement].price
                        isRateInHand = false
                    }
                }
                if index != segment.count - 1 {
                    rateStatus[rate] = RateStatus.buy.rawValue
                    profit -= rate.price
                    purchasedPrice = rate.price
                    isRateInHand = true
                }
            }
        }
        
        return (rateStatus, profit)
    }
    
    private static func isPit(mid: Int, left: Int, right: Int) -> Bool {
        mid < left && mid < right
    }
    
    private static func findFirstIndexWithMaxPriceIn(segment: [StockRate], fromIndex: Int, toIndex: Int) -> Int {
        let partOfSegment = Array(segment[fromIndex...toIndex])
        if let maxValue = partOfSegment.compactMap({ rate -> Int? in rate.price }).max() {
            return fromIndex + (partOfSegment.firstIndex(where: { rate -> Bool in
                rate.price == maxValue
            }) ?? 0)
        }
        return fromIndex
    }
}
