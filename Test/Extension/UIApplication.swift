//
//  UIApplication.swift
//  Test
//
//  Created by Egor Lindberg on 28.03.2021.
//

import UIKit.UIApplication

extension UIApplication {
    static var statusBarHeight: CGFloat {
        if #available(iOS 13.0, *) {
            let window = shared.windows.filter { $0.isKeyWindow }.first
            return window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            return shared.statusBarFrame.height
        }
    }
}
