//
//  UIView.swift
//  Test
//
//  Created by Egor Lindberg on 28.03.2021.
//

import UIKit

extension UIView {

    var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }

    var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }

    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        } set {
            layer.borderColor = newValue?.cgColor
        }
    }

    var shadowRadius: CGFloat {
        get { return layer.shadowRadius }
        set { layer.shadowRadius = newValue }
    }
    
    var shadowOffset : CGSize {
        get { return layer.shadowOffset }
        set { layer.shadowOffset = newValue }
    }

    var shadowColor : UIColor {
        get { return UIColor.init(cgColor: layer.shadowColor!) }
        set { layer.shadowColor = newValue.cgColor }
    }
    
    var shadowOpacity : Float {
        get { return layer.shadowOpacity }
        set { layer.shadowOpacity = newValue }
    }
    
}
