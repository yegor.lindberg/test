//
//  CustomButton.swift
//  Test
//
//  Created by Egor Lindberg on 29.03.2021.
//

import UIKit.UIButton

class CustomButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupView()
    }
    
    func setupView() {
        cornerRadius = self.bounds.height / 2
        backgroundColor = .orange
        setTitleColor(.white, for: .normal)
    }
    
}
