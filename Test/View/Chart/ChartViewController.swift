//
//  ChartViewController.swift
//  Test
//
//  Created by Egor Lindberg on 27.03.2021.
//

import UIKit
import SnapKit

class ChartViewController: UIViewController {
    
    private var chartView = ChartView()
    private var button = CustomButton(type: .system)
    
    private var chartViewModel = ChartViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMainView()
        setupButton()
        setupViewModel()
    }
    
    private func setupMainView() {
        view.backgroundColor = .white
        navigationItem.title = "Stock chart"
    }
    
    private func setupViewModel() {
        chartViewModel.onStockRatesLoaded = { [weak self] rates in
            guard let self = self else { return }
            self.drawChart(rates: rates ?? [])
        }
        chartViewModel.viewDidLoad(with: self.navigationController!)
    }
    
    private func setupButton() {
        button.setTitle("Show results", for: .normal)
        if #available(iOS 14.0, *) {
            button.addAction(UIAction(handler: { _ in
                self.chartViewModel.onButtonTouched()
            }), for: .touchUpInside)
        } else {
            button.addTarget(self, action: #selector(chartViewModel.onButtonTouched), for: .touchUpInside)
        }
        self.view.addSubview(button)
        
        button.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().inset(20)
            make.left.equalToSuperview().inset(15)
            make.centerX.equalToSuperview()
        }
    }
    
    private func drawChart(rates: [StockRate]) {
        self.view.addSubview(chartView)
        chartView.backgroundColor = #colorLiteral(red: 0.9311528802, green: 0.9245012403, blue: 0.9362466931, alpha: 1)
        chartView.cornerRadius = 10
        chartView.layer.masksToBounds = true
        
        chartView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.centerX.equalTo(self.view)
            make.top.equalTo((self.navigationController?.navigationBar.bounds.size.height ?? 0)
                                + UIApplication.statusBarHeight
                                + 10)
            make.height.equalTo(UIScreen.main.bounds.width)
        }
        
        chartView.data = rates.compactMap({ (stockRate) -> CGFloat in
            CGFloat(stockRate.price)
        })
    }
    
    
}

