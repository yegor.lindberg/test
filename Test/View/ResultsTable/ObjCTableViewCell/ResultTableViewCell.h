//
//  ResultTableViewCell.h
//  Test
//
//  Created by Egor Lindberg on 31.03.2021.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResultTableViewCell: UITableViewCell

+ (NSDictionary *)identifier;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *statusLabel;

@end

NS_ASSUME_NONNULL_END
