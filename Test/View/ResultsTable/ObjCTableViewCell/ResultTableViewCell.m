//
//  UITableViewCell+ResultTableViewCell.m
//  Test
//
//  Created by Egor Lindberg on 31.03.2021.
//

#import "ResultTableViewCell.h"

@implementation ResultTableViewCell
    
- (UILabel *)dateLabel {
    if (!_dateLabel) {
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.font = [UIFont systemFontOfSize: 17];
        _dateLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_dateLabel];
    }
    return _dateLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = [UIFont systemFontOfSize: 17];
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_priceLabel];
    }
    return _priceLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.font = [UIFont systemFontOfSize: 17];
        _statusLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_statusLabel];
    }
    return _statusLabel;
}

+ (NSString *)identifier {
  return @"ResultTableViewCell";
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self setupLabels];
    
    return self;
}

- (void) setupLabels
{
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@120);
        make.height.equalTo(@36);
        make.top.bottom.equalTo(@0);
        make.left.equalTo(@15);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@70);
        make.height.equalTo(@36);
        make.top.bottom.equalTo(@0);
        make.left.equalTo([_dateLabel mas_right]);
    }];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@36);
        make.top.bottom.equalTo(@0);
        make.left.equalTo([_priceLabel mas_right]);
        make.right.equalTo([self contentView]).with.offset(-20);
    }];
}
    
@end
