//
//  ResultsTableViewController.swift
//  Test
//
//  Created by Egor Lindberg on 29.03.2021.
//

import UIKit

class ResultsTableViewController: UIViewController {
    
    private(set) var viewModel: ResultsViewModel!
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(ResultTableViewCell.self,
                       forCellReuseIdentifier: "ResultTableViewCell")
        return table
    }()
    
    lazy var navBarHeight = (self.navigationController?.navigationBar.bounds.size.height ?? 0)
        + UIApplication.statusBarHeight
    
    public static func make(rates: [StockRate]) -> ResultsTableViewController {
        let vc = ResultsTableViewController()
        let viewModel = ResultsViewModel(rates: rates)
        vc.viewModel = viewModel
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMainView()
        setupTableView()
        setupResultLabel()
    }
    
    private func setupMainView() {
        view.backgroundColor = .orange
        navigationItem.title = "Results table"
    }
    
    private func setupResultLabel() {
        let resultLabel = UILabel()
        view.addSubview(resultLabel)
        setupLabelsText(for: [resultLabel])
        resultLabel.numberOfLines = 0
        resultLabel.text = viewModel.profit > 0
            ? "The maximum profit for the given period is \(viewModel.profit)."
            : "Data is not enough"
        resultLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        resultLabel.snp.makeConstraints { make in
            make.top.equalTo(tableView.snp.bottom).inset(5)
            make.left.equalToSuperview().inset(15)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(5)
        }
    }
    
    private func setupLabelsText(for labels: [UILabel]) {
        labels.forEach { label in
            label.textAlignment = .center
            label.textColor = .white
        }
    }
}


//MARK: - UITableViewDelegate, UITableViewDataSource and Table layout
extension ResultsTableViewController: UITableViewDelegate, UITableViewDataSource {
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.allowsSelection = false
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(navBarHeight + 40)
            make.left.equalToSuperview()
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(50)
        }
        addTableTitles()
    }
    
    private func addTableTitles() {
        let dateLabel = UILabel()
        let priceLabel = UILabel()
        let statusLabel = UILabel()
        view.addSubview(dateLabel)
        view.addSubview(priceLabel)
        view.addSubview(statusLabel)
        setupLabelsText(for: [dateLabel, priceLabel, statusLabel])
        dateLabel.text = "Date"
        dateLabel.snp.makeConstraints { make in
            make.top.equalTo(navBarHeight)
            make.left.equalToSuperview().inset(15)
            make.width.equalTo(120)
            make.bottom.equalTo(tableView.snp.top).inset(10)
        }
        priceLabel.text = "Price"
        priceLabel.snp.makeConstraints { make in
            make.top.equalTo(navBarHeight)
            make.left.equalTo(dateLabel.snp.right).inset(10)
            make.width.equalTo(80)
            make.bottom.equalTo(tableView.snp.top).inset(10)
        }
        statusLabel.text = "Status"
        statusLabel.snp.makeConstraints { make in
            make.top.equalTo(navBarHeight)
            make.left.equalTo(priceLabel.snp.right).inset(10)
            make.right.equalToSuperview().inset(15)
            make.bottom.equalTo(tableView.snp.top).inset(10)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.rates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTableViewCell",
                                                 for: indexPath) as! ResultTableViewCell
        cell.dateLabel.text = viewModel.rates[indexPath.row].date
        cell.priceLabel.text = String(viewModel.rates[indexPath.row].price)
        cell.statusLabel.text = viewModel.rateStatuses[viewModel.rates[indexPath.row]]
        return cell
    }
}
