////
////  ResultTableViewCell.swift
////  Test
////
////  Created by Egor Lindberg on 29.03.2021.
////
//
//import UIKit
//import SnapKit
//
//class ResultTableViewCell: UITableViewCell {
//    
//    public static let identifier = "resultCell"
//    var dateLabel = UILabel()
//    var priceLabel = UILabel()
//    var statusLabel = UILabel()
//    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        addSubview(dateLabel)
//        addSubview(priceLabel)
//        addSubview(statusLabel)
//        setupLabels()
//    }
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        
//    }
//
//    
//    private func setupLabels() {
//        dateLabel.textAlignment = .center
//        dateLabel.snp.makeConstraints { make in
//            make.top.equalToSuperview().inset(10)
//            make.left.equalToSuperview().inset(15)
//            make.width.equalTo(120)
//            make.bottom.equalToSuperview().inset(10)
//        }
//        priceLabel.textAlignment = .center
//        priceLabel.snp.makeConstraints { make in
//            make.top.equalToSuperview().inset(10)
//            make.left.equalTo(dateLabel.snp.right).inset(10)
//            make.width.equalTo(80)
//            make.bottom.equalToSuperview().inset(10)
//        }
//        statusLabel.textAlignment = .center
//        statusLabel.snp.makeConstraints { make in
//            make.top.equalToSuperview().inset(10)
//            make.left.equalTo(priceLabel.snp.right).inset(10)
//            make.right.equalToSuperview().inset(15)
//            make.bottom.equalToSuperview().inset(10)
//        }
//    }
//}
