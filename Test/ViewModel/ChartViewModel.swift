//
//  ChartViewModel.swift
//  Test
//
//  Created by Egor Lindberg on 28.03.2021.
//

import UIKit

final class ChartViewModel {
    
    var onStockRatesLoaded: (([StockRate]?) -> ())?
    
    let ratesHandler = RatesHandler()
    private var chartRouter: ChartRouter?
    private(set) var rates: [StockRate] = []
    
    func viewDidLoad(with navigationController: UINavigationController) {
        getStockRates()
        chartRouter = ChartRouter(with: navigationController)
    }
    
    func getStockRates() {
        ratesHandler.getStockRates() { [weak self] rates in
            guard let self = self else { return }
            self.rates = rates ?? []
            self.onStockRatesLoaded?(rates)
        }
    }
    
    @objc
    func onButtonTouched() {
        chartRouter?.route(to: .resultsTable, parameters: rates)
    }
}

