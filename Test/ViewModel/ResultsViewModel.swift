//
//  ResultsViewModel.swift
//  Test
//
//  Created by Egor Lindberg on 29.03.2021.
//

import Foundation

final class ResultsViewModel {
    
    var rates: [StockRate]!
    var rateStatuses: [StockRate: String]!
    var profit = 0
    
    init(rates: [StockRate]) {
        self.rates = rates
        let result = RateAlgorithm.calculateProfit(for: rates)
        self.rateStatuses = result.rateStatus
        self.profit = result.profit
    }
    
}
