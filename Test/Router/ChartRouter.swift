//
//  ChartRouter.swift
//  Test
//
//  Created by Egor Lindberg on 29.03.2021.
//

import UIKit

class ChartRouter: Router {
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    var navigationController: UINavigationController
    
    enum Route: CaseIterable {
        case resultsTable
    }
    
    func route(to view: Route, parameters: Any?) {
        switch view {
        case .resultsTable:
            if let rates = parameters as? [StockRate] {
                let resultsVc = ResultsTableViewController.make(rates: rates)
                navigationController.pushViewController(resultsVc, animated: true)
            } else {
                print(Self.self)
                fatalError(">>> ChartRouter ERROR: ResultsTableViewController need for [StockRates]. Send this in parameters.")
            }
        }
    }
    
    
}
