//
//  Router.swift
//  Test
//
//  Created by Egor Lindberg on 29.03.2021.
//
import UIKit.UINavigationController

protocol Router: AnyObject {
    associatedtype Route: CaseIterable
    func route(to view: Route, parameters: Any?)
    var navigationController: UINavigationController { get set }
}
