//
//  File.swift
//  
//
//  Created by Egor Lindberg on 28.03.2021.
//

import PromiseKit
import Foundation

protocol Api: AnyObject {
    func fetchAllStocks() -> Promise<Data>
}
