//
//  File.swift
//  
//
//  Created by Egor Lindberg on 28.03.2021.
//

import Foundation

public class Services {
    private init() {}
    
    public static let shared = Services()
    
    public let mockApiClient = MockApiClient()
    
}
