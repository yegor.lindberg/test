//
//  File.swift
//  
//
//  Created by Egor Lindberg on 28.03.2021.
//

import Foundation
import PromiseKit

final public class MockApiClient: Api {
    
    public func fetchAllStocks() -> Promise<Data> {
        return Promise { seal in
            readLocalFile(withName: "MockApi", completion: { data in
                if let jsonData = data {
                    seal.fulfill(jsonData)
                } else {
                    seal.reject(NSError(domain: "json is nil", code: 500, userInfo: nil))
                }
            })
        }
    }
    
    private func readLocalFile(withName: String, completion: @escaping (Data?) -> Void) {
        if let bundleUrl = Bundle.module.url(forResource: withName,
                                              withExtension: "json"),
           let jsonData = try? String(contentsOf: bundleUrl).data(using: .utf8) {
            completion(jsonData)
        } else {
            completion(nil)
        }
    }
}
