// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Services",
    platforms: [.iOS(.v12)],
    products: [
        .library(
            name: "Services",
            targets: ["Services"]),
    ],
    dependencies: [
        .package(name: "PromiseKit",
                 url: "https://github.com/mxcl/PromiseKit",
                 from: "6.13.2"),
    ],
    targets: [
        .target(
            name: "Services",
            dependencies: ["PromiseKit"],
            resources: [.process("Resources/MockApi.json")]),
        .testTarget(
            name: "ServicesTests",
            dependencies: ["Services"]),
    ]
)
